package jsonfile

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

// This package provides a very simple way to save any arbitrary struct
// as a JSON file to disk.
//
// Notice that due to the nature of how interfaces are implemented in Go,
// the json package expects a pointer to a struct, and so does this one.
//
// The use of JSON tags like `json:"foo_bar"` on each field is recommended
// for clarity.
//
// Lastly, there currently is no support for unexported fields - such are
// ignored and not written to nor read from disk.

// Save writes the contents of an arbitrary struct to disk as a JSON file. Returns
// an error if it failed to marshall it into JSON, or if it couldn't
// write it to disk.
func Save(s interface{}, filename string) error {
	jsonFile, err := json.MarshalIndent(s, "", "\t")
	if err != nil {
		return fmt.Errorf("couldn't marshall %v into json: %v", s, err)
	}

	err = ioutil.WriteFile(filename, jsonFile, 0644)
	if err != nil {
		return err
	}

	return nil
}

// Load reads the contents of a JSON file from disk and
// unmarshalls it into the given struct. Returns an error if the file
// couldn't be opened or read, or if the unmarshalling failed.
func Load(s interface{}, filename string) error {
	file, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer file.Close()

	data, err := ioutil.ReadAll(file)
	if err != nil {
		return err
	}

	err = json.Unmarshal(data, s)
	if err != nil {
		return fmt.Errorf("couldn't unmarshall %v: %v", s, err)
	}

	return nil
}
