package jsonfile

import (
	"os"
	"testing"
)

type Foo struct {
	Bar string
}

type Nice struct {
}

func TestSave(t *testing.T) {
	testFile := "test.json"
	testText := "something"

	file, err := os.Create(testFile)
	if err != nil {
		t.Error(err)
	}
	file.Close()
	defer os.Remove(testFile)

	foo := new(Foo)
	foo.Bar = testText

	err = Save(foo, testFile)
	if err != nil {
		t.Error(err)
	}

	foo.Bar = "Hello, 世界 & welcome!"
	err = Save(foo, testFile)
	if err != nil {
		t.Error(err)
	}
}
